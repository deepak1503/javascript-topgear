<script>

function allPossibilities(){
 
  // brute force solution
 
  // first calculate all possible combinations
  // of numbers and operators
  var mem = ["1"], combos;
  for(var y = 2; y <= 9; y++){
    combos = [];
    mem.forEach(function(x){
      combos.push(x + y, x + " +" + y, x + " -" + y);
    });
    mem = combos;
  }
 
  // Now filter out the ones that equal 100
  return combos.filter(function(combo){
    // split the combo into numbers, sum them using reduce
    
    return combo.split(" ").reduce(function(x,y){
      return x/1+y/1;
    }) == 100; // and check if the sum is 100
  })
  // format output by adding some spaces
  .map(function(x){
    return x.replace(/([+-])/g,'$1 ');
  });
 
}

x=allPossibilities()
for(i=0;i<x.length;i++)
document.write(x[i]+"<br>")

</script>