<script>
    x=parseInt(prompt("Enter an integer:"))
    var primes=new Array
    if(x>2)
    primes.push(2)
    
    if(x>3)
    primes.push(3)
    
    //Logic for checking whether prime or not
    for(i=4;i<x;i++)
    {
        flag=0
        for(y=2;y<=i/2;y++)
        {
            if(i%y==0)
            flag=1
        }
        
        if(flag==0)
        primes.push(i)
    }
    
    //Displaying the primes of the range between 1 and entered integer
    for(x=0;x<primes.length;x++)
    {
        document.write(primes[x]+" ")
    }
</script>